program make_bio_obc

  implicit none
  integer :: i,t
  real,dimension(30)::bio

  bio=7.0
  open(1,file='gom_bio_obc.dat',status='replace')
  do t=1,10
     write(1,*)(t-1)*10.0
     write(1,'(a)')'NO\Lay         1         2         3         4         5         6         7         8         9        10        11        12        13        14        15        16        17        18        19        20        21        22        23        24        25        26        27        28        29        30'
     do i=1,77
        write(1,'(i7,30f6.2)')i,bio(1:30)
     end do
  end do


  close(1)

end program make_bio_obc
